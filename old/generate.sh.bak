## Author: Jose Mendoza (joedoe47)
## mksite v2
##  A hopefully more efficent static site generator written in bash
##
## Requires: cmark, sed, git, coreutils, and bash for max compability

## User defined variables
SITE_NAME="${SITE_NAME:-Mendoza Consultant}"
SITE_URL="${SITE_URL:-mendozaconsultcorp.com}" #(https:// is added for us)
AUTHOR="$(printf %s "$(git config user.name)")" #this allows for blog like posting for multiple users
GRAVATAR="$(printf %s "$(git config user.email)" | md5sum | cut -d ' ' -f 1)" #avatars for usersi
GRAVATAR_SIZE="200" #avatar size in pixels
DATE="$(date +'%r | %F' -u)" #date/time in UTC format (gives a little privacy)

## Variables that should be left alone but can be modified ##

#Template files (0 - header | 1 - menu | 2 - comment/end of article | 3 - footer ; file.md is added automatically)
BODY=("header.html" "menu.html" "comment.html" "footer.html")

# Base folder Structure (you can rename them)
FOLDERS=("Output" "Template" "Source")

# Folders we need in Source/ and Output/( 0 - posts | 1 - archive | 2 - assests )
SOURCE_TEMPLATE=("posts" "archive" "assests")

# Spacing between prompts
NOSPACE="%s"
TRIPLE="%s\\n\\n\\n"
DOUBLE="%s\\n\\n"
SINGLE="%s\\n"

#Format for each new post/page (tags are a WIP but they can still be used by SEOs)
NO_FILE_NAME="${2%.*}"
POST_FORMAT="## ${NO_FILE_NAME// /_}

> **![avatar](GRAVATAR)**

> Author: $AUTHOR

> Date: $DATE

> Tags:

----------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec faucibus est ut magna dignissim, vel eleifend augue facilisis. Aenean dapibus diam nec metus placerat, vitae mattis nisi elementum. In venenatis mi vel ultrices pretium. Nam suscipit ante eget ullamcorper porta. Proin consequat scelerisque scelerisque. Cras at felis enim. Donec blandit neque vitae volutpat iaculis. Proin vehicula ipsum at erat egestas pretium. Vestibulum posuere eros in dui convallis ultrices. Donec lacinia leo risus, eu sodales eros porta quis. Curabitur maximus volutpat odio, vel auctor odio tempus ac. Vivamus mollis in justo convallis tempus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam a cursus augue."

## Blog post format
#> [test](https://example.com/posts/test.html) (joedoe47 | 6:40:03 PM | 04-02-18)
INDEX_FORMAT="> [${NO_FILE_NAME// /_}](/${SOURCE_TEMPLATE[0]}//${NO_FILE_NAME// /_}.html) (by: $AUTHOR | $DATE)"

## Test if dependancies are met
DEPENDANCY=("cmark" "sed" "optipng" "convert")
for PKG in ${DEPENDANCY[@]}; do
hash ${PKG} 2>/dev/null || { echo >&2 "I require ${PKG} but it's not installed. Aborting."; } #alert but do not exit as user can decide to change cmark for pandoc, redcarpet, md2html.awk, etc. (need to find a more better way to check but allow user choice)
done

## check of ${BODY[@]}, ${SOURCE_TEMPLATE[@]}, ${BODY[@]} and load files to ram
#test if folder structure exists
for FOLDER in "${FOLDERS[@]}"; do
test -d "$FOLDER" || { printf ${DOUBLE} "$FOLDER not found. creating." ; mkdir "$FOLDER" ;}
done
unset FOLDER

#Source/ folder structure
for FOLDER in "${SOURCE_TEMPLATE[@]}"; do
test -d "${FOLDERS[2]}/$FOLDER" || { printf ${DOUBLE} "${FOLDERS[2]}/$FOLDER not found. creating." ; mkdir -p "${FOLDERS[2]}/$FOLDER" ;}
done
unset FOLDER

#Output/ folder structure
for FOLDER in "${SOURCE_TEMPLATE[@]}"; do
test -d "${FOLDERS[0]}//$FOLDER" || { printf ${DOUBLE} "${FOLDERS[0]}/$FOLDER not found. creating." ; mkdir -p "${FOLDERS[0]}/$FOLDER" ;}
done
unset FOLDER

#can not put a vairable in [] so we have to execute each one manually
test -s "${FOLDERS[1]}/${BODY[0]}" || { printf ${SINGLE} "${FOLDERS[1]}/${BODY[0]} is missing. Please run '$0 template' to fix this."; exit 1 ; }
test -s "${FOLDERS[1]}/${BODY[1]}" || { printf ${SINGLE} "${FOLDERS[1]}/${BODY[0]} is missing. Please run '$0 template' to fix this."; exit 1 ; }
test -s "${FOLDERS[1]}/${BODY[2]}" || { printf ${SINGLE} "${FOLDERS[1]}/${BODY[0]} is missing. Please run '$0 template' to fix this."; exit 1 ; }
test -s "${FOLDERS[1]}/${BODY[3]}" || { printf ${SINGLE} "${FOLDERS[1]}/${BODY[0]} is missing. Please run '$0 template' to fix this."; exit 1 ; } 

#Prepare to generate. Cache contents of BODY[@] (header, menu, comment, footer) to ram.
for FILE in "${BODY[@]}"; do
#this is more efficient than other scripts because:
# - substitutions/plug ins executed at file load time
# - files loaded into ram for faster subsequent generation of files
# - additional substitutions/plug ins can be added with another -e "s/find/replace/g"
# Current template "Plug ins": Minify, {SITE_NAME}, {SITE_URL}, {AUTHOR}
# 
declare ${FILE%.*}="$(cat "${FOLDERS[1]}/${FILE}" | sed -e ':a;N;$!ba;s/>\s*</></g' -e "s/{SITE_URL}/https\:\/\/${SITE_URL}/g" -e "s/{SITE_NAME}/${SITE_NAME}/g" -e "s/{DATE}/${DATE}/g" -e "s/{AUTHOR}/${AUTHOR}/g" -e "s/{GRAVATAR}/https\:\/\/www.gravatar.com\/avatar\/${GRAVATAR}\?\=${GRAVATAR_SIZE}/g")"

# useful for debugging
#printf ${SINGLE} "${FILE%.*}"
done


## Functions

## Generate html
assemble () {
#fix paths 
declare temp="${1##*${FOLDERS[2]}/}" #we need subfolders post/ assests/ etc/
declare page="${1##*/}" #we need only the name of "file.md"

#test if $1 already exists, else exit (saves on CPU cycles; especially if you have thousands of posts/pages)
#downside: need to delete files we want to regen
test -f "${FOLDERS[0]}/${temp%.*}.html" && { printf ${DOUBLE} "${FOLDERS[0]}/${temp%.*}.html aleady exists. Skipping."; return 0;}

#stage, assemble, and generate "file.md" to "file.html"
#This is where perhaps more useful plug ins can be added. (again with a simple -e "/s/find/replace/g")
# Current page "plug ins": minify, {AUTHOR}, {PAGE}, {DATE}
declare OUTPUT="$(printf ${NOSPACE} "${header}${menu}$(cmark "${1}")${comment}${footer}" | sed -e ':a;N;$!ba;s/>\s*</></g' -e "s/{PAGE}/${page%.*}/g" -e "s/GRAVATAR/https\:\/\/www.gravatar.com\/avatar\/${GRAVATAR}\?\=${GRAVATAR_SIZE}/g" -e "s/{AUTHOR}/${AUTHOR}/g" -e "s/{DATE}/${DATE}/g" )"

#save html to file in OUTPUT ${FOLDERS[0]}/file.html (generates a giant verbose mess)
printf ${NOSPACE} "${OUTPUT}" > ${FOLDERS[0]}/${temp%.*}.html

unset page
unset temp
}


## Search for files and folders in Source/ ${FOLDERS[2]} (default 2 sub directories)
#find was not used because I thought this method would be more efficent. 
# currently only support markdown (md) files
#also create directories just in case if user decides to make their own directory layout
#the '##' inside variables is to fix certain paths to put files/folders in the 'Output' folder (${FOLDERS[0]} not in 'Source' (${FOLDERS[2]})
search_folders () {
#Check Source/ folder
for ENTRY in ${FOLDERS[2]}/*; do
#file found assmble
test -f "$ENTRY" && { if [ "${ENTRY}" != "${ENTRY%.md}" ]; then printf ${SINGLE} "file found. ${ENTRY}"; assemble "${ENTRY}"; fi ;} #assemble "Source/file.md"
#if a sub folder is found try and create it in Output, ${FOLDERS[0]}
test -d "$ENTRY" && { printf ${SINGLE} "folder found. ${ENTRY}" ; mkdir -p ${FOLDERS[0]}/${ENTRY##${FOLDERS[2]}/} ;} #mkdir Output/subfolder/
done

#subfolder 1
for ENTRY in ${FOLDERS[2]}/*/*; do
#file found assmble
test -f "$ENTRY" && { if [ "${ENTRY}" != "${ENTRY%.md}" ]; then printf ${SINGLE} "file found. ${ENTRY}"; assemble "${ENTRY}"; fi ;} #assemble "Source/subfolder/file.md"
#if a sub folder is found try and create it in Output, ${FOLDERS[0]}
test -d "$ENTRY" && { printf${SINGLE} "folder found. ${ENTRY}" ; mkdir -p ${FOLDERS[0]}/${ENTRY##${FOLDERS[2]}/} ;} #mkdir Output/subfolder/subfolder/
done

#subfolder 2
for ENTRY in ${FOLDERS[2]}/*/*/*; do
#file found assmble
test -f "$ENTRY" && { if [ "${ENTRY}" != "${ENTRY%.md}" ]; then printf ${SINGLE} "file found. ${ENTRY}"; assemble "${ENTRY}"; fi ;} #assemble "Source/subfolder/subfolder/file.md"
done
#additional sub folder processing can be added

printf ${DOUBLE} "markdown to html generation complete. "
}

## Move Files only from assests folder (posts, archive, and root should really be neat and tidy anyways)
move_files () {
printf ${DOUBLE} "Moving Assests folder. This might take a moment."
#cp -rv ${FOLDERS[2]}/${SOURCE_TEMPLATE[2]}/* ${FOLDERS[0]}/${SOURCE_TEMPLATE[2]}
rsync -P ${FOLDERS[2]}/${SOURCE_TEMPLATE[2]}/* ${FOLDERS[0]}/${SOURCE_TEMPLATE[2]}
}


## Check Input
check_input () {
if [[ ${1} == *['!''?''=''\'';'':''>''<'',''.''`''~'@#\$%^\&*()\[\]\{\}_+/]* ]]; then
printf ${DOUBLE} "No special characters are allowed!"
exit 1;
fi
}

## New Page
new_page () {
printf ${TRIPLE} "${POST_FORMAT}" > ${FOLDERS[2]}/${1// /_}.md
}

## New Blog Post
new_post () {
printf ${TRIPLE} "${POST_FORMAT}" > ${FOLDERS[2]}/${SOURCE_TEMPLATE[0]}/${1// /_}.md

#add entry to index.md but do not over write index.md
printf ${DOUBLE} "${INDEX_FORMAT}" >> ${FOLDERS[2]}/index.md
}

##Base64 images
mkbase64 () {
magick convert "${1}" -strip -interlace Plane -gaussian-blur 0.05 -quality 80% -colorspace RGB ${1%.*}.jpg
CONVERT="$(cat ${1%.*}.jpg | base64 -w 0)"
test -f "$1" && { printf ${DOUBLE} "${1} converted to ${1%.*}.jpg" ;printf ${SINGLE} "Markdown:"; printf ${TRIPLE} "![${1%.*}.jpg](data:image/png;base64,${CONVERT})"; printf ${SINGLE} "HTML:"; printf ${TRIPLE} "<img alt=\"${1%.*}.jpg\" src=\"data:image/png;base64,${CONVERT}\">"; printf ${SINGLE} "CSS: "; printf ${TRIPLE} "div.${1%.*} { background-image:url(data:image/png;base64;${CONVERT}); }" ;} || { printf ${SINGLE} "$1 does not exist"; exit 1;}
}

## Clean old html files
clean_output() {
printf ${DOUBLE} "Cleaning all html files${FOLDERS[0]}"
#delete within first 2 subfolders
rm ${FOLDERS[0]}/*.html
rm ${FOLDERS[0]}/*/*.html
rm ${FOLDERS[0]}/*/*/*.html

#remove empty folders
rmdir ${FOLDERS[0]}/*/*
rmdir ${FOLDERS[0]}/*/*/*

}

##Server
httpd_server () {
cd ${FOLDERS[0]}; python -m SimpleHTTPServer "${1:-8080}" #start on 8080 unless specified
}

## Generate sample template
#might use a more generic sample template rather than bootstrap (it looks nice but not necessarily great for privacy reasons; unless css is self hosted)
gen_sample () {
#if TEMPLATE/header,menu,comment,footer does not exist make a basic html5 bootstrap site
test -s "${FOLDERS[1]}/${BODY[0]}" || { printf ${SINGLE} "${FOLDERS[1]}/${BODY[0]} is missing. creating.";
printf ${SINGLE} "<!DOCTYPE html>
<html lang=\"en\">
<head>
<title>{SITE_NAME} | {PAGE}</title>
<meta charset=\"utf-8\">
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\", shrink-to-fit=no\">
<meta name=\"description\" content=\"\">
<meta name=\"author\" content=\"\">
<!-- bootstrap 4.0 - markdown specific css - fontawesome -->
<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css\" integrity=\"sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm\" crossorigin=\"anonymous\">

<link rel=\"stylesheet\" href=\"../../assests/md.css\">

<link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v5.1.0/css/all.css\" integrity=\"sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt\" crossorigin=\"anonymous\">

</head><body>
<nav class=\"navbar navbar-expand-lg navbar-dark bg-dark sticky-top\">
<div class=\"container-fluid\">
<div class=\"navbar-header\">
<button type=\"button\" class=\"navbar-toggler\" data-toggle=\"collapse\" data-target=\"#myNavbar\" aria-controls=\"navbarResponsive\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
<span class=\"navbar-toggler-icon\"></span>                        
</button>
<a class=\"navbar-brand\" href=\"{SITE_URL}\">{SITE_NAME}</a>
</div>

<div class=\"collapse navbar-collapse\" id=\"myNavbar\">" > ${FOLDERS[1]}/${BODY[0]}; }

test -s "${FOLDERS[1]}/${BODY[1]}" || { printf ${SINGLE} "${FOLDERS[1]}/${BODY[1]} is missing. creating.";
printf ${SINGLE} "<ul class=\"navbar-nav navbar-right ml-auto\">

<li class=\"nav-item\"> 
<a class=\"nav-link\" href=\"../../index.html\">Home</a> 
</li>

<li class=\"nav-item\">
<a class=\"nav-link\" href=\"../../about.html\">About</a>
</li>

<li class=\"nav-item\">
<a class=\"nav-link\" href=\"../../contact.html\">Contact</a>
</li>

<li class=\"nav-item\">
<a class=\"nav-link\" href=\"https://git.joepcs.com\">Git</a>
</li>

</ul>

<!-- search duckduckgo -->
<form class=\"form-inline\" action=\"https://duckduckgo.com/\" method=\"POST\">
<input class=\"form-control mr-sm-2\" type=\"text\" placeholder=\"Search\" aria-label=\"Search\" name=\"q\">
<input type=\"hidden\" name=\"sites\" value=\"{SITE_URL}\"><button class=\"btn btn-success\" type=\"submit\">Search</button>
</form>

</div>
</div>
</nav>

<div class=\"container\">

<div class=\"row\">

<div class=\"col-md-8 card-body\">
<!-- page content starts here -->" > ${FOLDERS[1]}/${BODY[1]}; }

test -s "${FOLDERS[1]}/${BODY[2]}" || { printf ${SINGLE} "${FOLDERS[1]}/${BODY[2]} is missing. creating.";
printf ${SINGLE} "</div>
<!-- page content ends here -->

<!-- Sidebar Widgets Column -->
<div class=\"col-md-3\">

<!-- Categories Widget -->
<div class=\"card my-3\">
<h5 class=\"card-header\">Sites of Interest</h5>
<div class=\"card-body\">
<div class=\"row\">
<div class=\"col-lg-6\">
<ul class=\"list-unstyled mb-0\">
  <li><a href=\"#\">Web Design</a></li>
  <li><a href=\"#\">HTML</a></li>
  <li><a href=\"#\">Freebies</a></li>
</ul>
</div>

<div class=\"col-lg-6\">
<ul class=\"list-unstyled mb-0\">
  <li><a href=\"#\">JavaScript</a></li>
  <li><a href=\"#\">CSS</a></li>
  <li><a href=\"#\">Tutorials</a></li>
</ul>
</div>
</div>
</div>
</div>

<!-- Side Widget -->
<div class=\"card my-3\">
<h5 class=\"card-header\">Please Support</h5>
<div class=\"card-body\">
<div class=\"row\">
<div class=\"col-md-6\">
<i class=\"fab fa-btc\"></i>
<i class=\"fab fa-paypal\"></i>
<i class=\"fab fa-gratipay\"></i>
</div>
<div class=\"col-md-6\">
<i class=\"fab fa-ethereum\"></i>
<i class=\"fab fa-patreon\"></i>
</div>
</div>
</div>
</div>

<!--end of containers-->
</div>
</div>

<!-- code for comments here -->" > ${FOLDERS[1]}/${BODY[2]}; }

test -s "${FOLDERS[1]}/${BODY[3]}" || { printf ${SINGLE} "${FOLDERS[1]}/${BODY[3]} is missing. creating.";
printf ${SINGLE} "<footer class=\"container\">
<hr>                                                                                                                                                                          
<p>{PAGE} generated {DATE}. <a href=\\"#\\">Back to top</a></p>                                                                                                                                   
<p>&copy; 2017-2018 Company, Inc. &middot; <a href=\"#\">Privacy</a> &middot; <a href={GIT_SOURCE}>Source</a></p> 

</footer>
<!-- jquery, popper, and bootstrapjs -->
<script src=\"https://code.jquery.com/jquery-3.2.1.slim.min.js\" integrity=\"sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN\" crossorigin=\"anonymous\"></script>
<script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js\" integrity=\"sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q\" crossorigin=\"anonymous\"></script>
<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js\" integrity=\"sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl\" crossorigin=\"anonymous\"></script>

</body></html>" > ${FOLDERS[1]}/${BODY[3]}; }
}

## Help
help () {
printf ${SINGLE} "$0
Usage: $0 [parameter]

help                   This dialouge

page \"name\"          New page (edit $0 to edit template format \"\$POST_FORMAT\")

post \"name\"          New post (edit $0 to edit template format \"\$POST_FORMAT\" and \"\$INDEX_FORMAT\")

template               Generate a sample template for this site

generate               Generate site from ${FOLDERS[2]}

clean                  Delete html files from ${FOLDERS[0]} (Output) folders to allow re-generation (does not delete assests or other files)

base64 \"file.jpg\"    Get \"file.jpg\", minimize, and convert an image into base64 css (best used with small static images)

http 8080               Start simple http server on port 8080 to see results (default: 8080)

-----

Template Variables
  Consider setting these variables in $0 or just export them to your terminal

{SITE_NAME} = $SITE_NAME
{SITE_URL} = $SITE_URL
{AUTHOR} = $AUTHOR
{GIT_SOURCE} - $GIT_SOURCE
{PAGE} = *.md (show the name of the md file without the extention)
"
}

case "$1" in
generate) move_files ; search_folders ;;
template) gen_sample ;;
page) check_input "${2}"; new_page "${2}" ;;
post) check_input "${2}"; new_post "${2}";;
clean) clean_output ;;
base64) mkbase64 "${2}" ;;
http) httpd_server "${2}" ;;
*) help ;;
esac
#EOF
