#terminal + tmux for fun and profit

I was recently talking with a few highschool friends who wanted to get into hacking but were unsure why it was important to get used to the terminal (specifically in Linux or BSD but heck the terminal can be useful in OsX and Windows too)

All three of them were stunned to find out I walk around with a raspberry pi and an intel compute stick but I don't always need to even boot a GUI. I can see facebook messenger, edit code, manager 5+ machines from anywhere, chat on IRC`

Unless I want to play video games or specifically watch videos on youtube, steemit, or crunchyroll.

There are some immediate benefits we should be aware of when using the terminal:
- It uses the least resources (ram, cpu, and spinning disk hard drive read/write)

- Its used for max efficiency for certain applications

- It can be automated (aka. scripted) to do really boring but essential tasks 

- It can make you overall more productive (less distracting graphics on lynx, elinks, w3m and keyboard shotcuts might make your work flow more efficent)

This alone is why many system admins, some developers/hackers, and tech hobbyists (like people who mine bit/alt coins) will prefer using a boring text only interface.

Of course there are some obstacles like:

- A **steep* learning curve (it can be a challenge to memorize a bunch of key shortcuts)

- some scripts are specific to certain enviornments (eg. if you make a script to compile/assemble a program in Solaris, the script will probably not work on a linux variant like Ubuntu; but might possble with some editing and possible workarounds)

- Reading/Writing files with a program like vim, nano, etc.

- 

- inability to see certain javascript enabled websites or certain graphics in PDFs 

but I assure you if you really want to reap the benefits, the struggle is worth it and if you want to use more standard keys byobu is also another way to use tmux/screen.



