<div class="content-wrapper">
<div class="content">

## privacy

> **![avatar](GRAVATAR)**

> Author: joedoe47

> Date: 04:27:35 AM | 2018-07-02

----------

## Privacy Policy

Cookies are used for generic information storage and are not really used in this site other than say to provide non-intrusive and non-user targeted ads. Users are free to block any cookies or information that specifically identifies them. Any client or server side code is accessible to inspect that I am not using any tricks to obtain user data. User privacy is important, I would not like to be tracked, spied, or tricked so I treat others with the same respect. 

I do keep logs of users who access information but I use it as metric to determine my audience and how to better distribute content, available [here](../../logs/mcm.html).

Should there be something you think is erronous or mishandled, please feel free to send me an email at joedoe47 at gmail dot com.

Sincerely, 

Jose A Mendoza


</div>
