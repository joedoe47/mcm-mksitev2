## Author: Jose Mendoza (joedoe47)
## mksite v2
##  A hopefully more efficent static site generator written in bash
##
## Requires: cmark, sed, git, coreutils, and bash for max compability

# Source configuration file if it exists in the project directory
if [ -f "config.sh" ]; then
  . "config.sh"
else 
cat <<EOF > config.sh
#Mksite Configuration

SITE_NAME="Mksite"

SITE_URL="example.com"
#SITE_URL="example.com"

#leave commented if multiple users make posts
#AUTHOR="John Doe"

GRAVATAR_SIZE="90"

#files that you want copied over to the output folder
STATIC_FILES=(
"htm"
"html"
"css"
"js"
"jpeg"
"jpg"
"png"
"gif"
"bmp"
)

#the default file ending to convert into webpages
TEXT_FILE="md"

#add http freindly url if you use SSH
GIT_SOURCE="https://gitlab.com/joedoe47/mksitev2"
EOF
fi

## User defined variables

#name of site
SITE_NAME="${SITE_NAME:-Mksite}"

#URL of the site (no need for slashes)
SITE_URL="${SITE_URL:-127.0.0.1:8080}"

#this allows for blog like posting for multiple users if the config is left blank
AUTHOR="${AUTHOR:-$(printf %s "$(git config user.name)")}" 

#get gravatar from git email committer
GRAVATAR="${GRAVATAR:-$(printf %s "$(git config user.email)" | md5sum | cut -d ' ' -f 1)}" 

#avatar size in pixels
GRAVATAR_SIZE="${GRAVATAR_SIZE:-90}" 

#get freindly git clone url from user otherwise just clone the default master
GIT_SOURCE="${GIT_SOURCE:-$(git remote get-url origin)}" 

#date/time in UTC format (gives a little privacy and standardizes if multiple users are on different time zones)
#DATE="$(date +'%r on %F' -u)"
DATE="$(date +'%c' -u)"

TEXT_FILE="${TEXT_FILE:-md}" #markdown is what we are normally looking for but this can be changed

## ---Variables that should be left alone but can be moified in this file--- ##

#Template files 
##
# file.md is added in between menu.html and comment.html
# 0 - header 
# 1 - menu 
# 2 - comment/end of article 
# 3 - footer  
BODY=("header.html" "menu.html" "comment.html" "footer.html")

# Base folder Structure 
##
# 0 - Output
# 1 - Template 
# 2 - Source
FOLDERS=("Output" "Template" "Source")

# Folders 
##
# 0 - posts 
# 1 - archive 
# 2 - assets 
SOURCE_TEMPLATE=("posts" "archive" "assets")

# Sed Scripts
##
# 0- minify         - Removes unecessary spaces and new lines
# 1- {{GRAVATAR}}   - Gravatar from email in git config
# 2- {{AUTHOR}}     - User in git config
# 3- {{DATE}}       - Today's date
# 4- {{SITE_URL}}   - URL of the sites
# 5- {{SITE_NAME}}  - Name of the site
# 6- {{PAGE}}       - Name of the page being assembled
# 7- {{GIT_SOURCE}} - shows git freindly url where to clone this or your project
SCRIPTS=(
    ':a;N;$!ba;s/>\s*</></g' 
    "s|{{GRAVATAR}}|<img src='https\://www.gravatar.com/avatar/${GRAVATAR}' height='${GRAVATAR_SIZE}' width='${GRAVATAR_SIZE}'>|g" 
    "s/{{AUTHOR}}/${AUTHOR}/g"  
    "s;{{DATE}};${DATE};g"    
    "s;{{SITE_URL}};${SITE_URL};g" 
    "s;{{SITE_NAME}};${SITE_NAME};g" 
    "s/{{PAGE}}/${page%.*}/g"
    "s;{{GIT_SOURCE}};${GIT_SOURCE};g"
    )

# Format for each new post/page 
NO_FILE_NAME="${2%.*}" #this variable should be left alone
POST_FORMAT="## ${NO_FILE_NAME// /_}

> **![$AUTHOR Gravatar](https://s.gravatar.com/avatar/$GRAVATAR?s=$GRAVATAR_SIZE)**

> Author: $AUTHOR

> Date: $DATE

> Tags: 

> ----------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec faucibus est ut magna dignissim, vel eleifend augue facilisis. Aenean dapibus diam nec metus placerat, vitae mattis nisi elementum. In venenatis mi vel ultrices pretium. Nam suscipit ante eget ullamcorper porta. Proin consequat scelerisque scelerisque. 


Cras at felis enim. Donec blandit neque vitae volutpat iaculis. Proin vehicula ipsum at erat egestas pretium. Vestibulum posuere eros in dui convallis ultrices. Donec lacinia leo risus, eu sodales eros porta quis. Curabitur maximus volutpat odio, vel auctor odio tempus ac. Vivamus mollis in justo convallis tempus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam a cursus augue."

## Blog post format
#TODO: find a way to auto archive posts
INDEX_FORMAT="<ul><b><a href=\"/${SOURCE_TEMPLATE[0]}/${NO_FILE_NAME// /_}.html\">${NO_FILE_NAME// /_}</a></b> by: $AUTHOR at $DATE</ul>"

# Spacing between prompts
NOSPACE="%s"
TRIPLE="%s\\n\\n\\n"
DOUBLE="%s\\n\\n"
SINGLE="%s\\n"

## ---Functions--- ##

# Test if dependancies are met
DEPENDANCY=("sed" "optipng" "convert" "git" "cmark")
for PKG in ${DEPENDANCY[@]}; do
hash ${PKG} 2>/dev/null || { echo >&2 "I require ${PKG} but it's not installed. Aborting."; } 
done

# Test if there is a git repository
test -d ".git" || { prinf ${DOUBLE} "not in a git repository. creating."; git init; }

## check of ${BODY[@]}, ${SOURCE_TEMPLATE[@]}, ${BODY[@]} and load files to ram
#test if folder structure exists
for FOLDER in "${FOLDERS[@]}"; do
test -d "$FOLDER" || { printf ${DOUBLE} "$FOLDER not found. creating." ; mkdir "$FOLDER" ;}
done
unset FOLDER

#Source/ folder structure
for FOLDER in "${SOURCE_TEMPLATE[@]}"; do
test -d "${FOLDERS[2]}/$FOLDER" || { printf ${DOUBLE} "${FOLDERS[2]}/$FOLDER not found. creating." ; mkdir -p "${FOLDERS[2]}/$FOLDER" ;}
done
unset FOLDER

#Output/ folder structure
for FOLDER in "${SOURCE_TEMPLATE[@]}"; do
test -d "${FOLDERS[0]}//$FOLDER" || { printf ${DOUBLE} "${FOLDERS[0]}/$FOLDER not found. creating." ; mkdir -p "${FOLDERS[0]}/$FOLDER" ;}
done
unset FOLDER

#Alert if Template files are missing, try to load them into a variable, and apply some sed scripts
for FILE in "${BODY[@]}"; do
    #see if a template file is missing before we do anything
    test -f "${FOLDERS[1]}/${FILE}" || { printf ${SINGLE} "${FOLDERS[1]}/${FILE} is missing. Please run '$0 template' to fix this."; }

    #1. make a new variable called header, menu, comment, footer
    #2. apply sed script to vairables above
    declare ${FILE%.*}="$(cat "${FOLDERS[1]}/${FILE}" | sed -e ${SCRIPTS[0]} -e "${SCRIPTS[1]}" -e "${SCRIPTS[2]}" -e "${SCRIPTS[3]}" -e "${SCRIPTS[4]}" -e "${SCRIPTS[5]}")"

done


## Functions

# Generate html
assemble () {
#fix paths 
declare temp="${1##*${FOLDERS[2]}/}" #we need subfolders post/ assets/ etc/
declare page="${1##*/}" #we need only the name of "file.md"

#test if $1 already exists, else exit (saves on CPU cycles; especially if you have thousands of posts/pages)
#downside: need to delete files we want to regen
test -f "${FOLDERS[0]}/${temp%.*}.html" && { printf ${DOUBLE} "${FOLDERS[0]}/${temp%.*}.html aleady exists. Skipping."; return 0;}

#stage, assemble, and generate "file.md" to "file.html"
#This is where perhaps more useful plug ins can be added. (again with a simple -e "/s/find/replace/g")
# Current page "plug ins": minify, {AUTHOR}, {PAGE}, {DATE}

declare OUTPUT="$(printf ${NOSPACE} "${header}${menu}$(cmark "${1}")${comment}${footer}" | sed -e ${SCRIPTS[0]} -e "${SCRIPTS[1]}" -e "${SCRIPTS[2]}" -e "${SCRIPTS[3]}" -e "${SCRIPTS[4]}" -e "${SCRIPTS[5]}" -e "${SCRIPTS[6]}")"

#save html to file in OUTPUT ${FOLDERS[0]}/file.html (generates a giant verbose mess)
printf ${NOSPACE} "${OUTPUT}" > ${FOLDERS[0]}/${temp%.*}.html

unset page
unset temp
}


## Search for files and folders in Source/ ${FOLDERS[2]} (default 2 sub directories)
# currently only support markdown (md) files
#also create directories just in case if user decides to make their own directory layout
search_folders () {
#Check Source/ folder

#Make folders that are missing
for ENTRY in $(find ${FOLDERS[2]}/* -type d); do
    printf ${DOUBLE} "Creating folders ${ENTRY}"
    mkdir -p ${FOLDERS[0]}/${ENTRY##${FOLDERS[2]}/}
done

#Get markdown or text files and make them into webpages
for ENTRY in $(find ${FOLDERS[2]} -type f | grep [$.]${TEXT_FILE}); do
    printf ${DOUBLE} "Assembling ${ENTRY}."
    assemble "${ENTRY}"
done
}

## Move Files only from assets folder (posts, archive, and root should really be neat and tidy anyways)
move_files () {
printf ${DOUBLE} "Moving Assests folder. This might take a moment."
rsync -P ${FOLDERS[2]}/${SOURCE_TEMPLATE[2]}/* ${FOLDERS[0]}/${SOURCE_TEMPLATE[2]}

printf ${DOUBLE} "Moving custom files"

for MIME in ${STATIC_FILE}; do
#nested loops are bad but unavilable at times
test -z ${MIME} && { printf ${SINGLE} "blank file type check config.sh"; exit 1 ; }
    for ENTRY in $(find ${FOLDERS[2]} -type f | grep [$.]${MIME}); do
        cp "${ENTRY}" ${FOLDERS[0]}/${ENTRY##${FOLDERS[2]}/}
    done
done
}


## Check Input
check_input () {
if [[ ${1} == *['!''?''=''\'';'':''>''<'',''.''`''~'@#\$%^\&*()\[\]\{\}_+/]* ]]; then
printf ${DOUBLE} "No special characters are allowed!"
exit 1;
fi
}

## New Page
new_page () {
printf ${TRIPLE} "${POST_FORMAT}" > ${FOLDERS[2]}/${1// /_}.md
}

## New Blog Post
new_post () {
printf ${TRIPLE} "${POST_FORMAT}" > ${FOLDERS[2]}/${SOURCE_TEMPLATE[0]}/${1// /_}.md

#add entry to index.md but do not over write index.md
printf ${DOUBLE} "${INDEX_FORMAT}" >> ${FOLDERS[2]}/index.md
}

##Base64 images
mkbase64 () {
CONVERT="$(cat ${1%.*}.jpg | base64 -w 0)"

test -f "$1" && { 
    magick convert "${1}" -strip -interlace Plane -gaussian-blur 0.05 -quality 75% -colorspace RGB ${1%.*}.jpg ;
    printf ${DOUBLE} "${1} converted to ${1%.*}.jpg"; 
    printf ${SINGLE} "Markdown:"; 
    printf ${TRIPLE} "![${1%.*}.jpg](data:image/png;base64,${CONVERT})"; 
    printf ${SINGLE} "HTML:"; 
    printf ${TRIPLE} "<img alt=\"${1%.*}.jpg\" src=\"data:image/png;base64,${CONVERT}\">"; 
    printf ${SINGLE} "CSS: "; 
    printf ${TRIPLE} "div.${1%.*} { background-image:url(data:image/png;base64;${CONVERT}); }" ; 
    printf ${SINGLE} "Note: This should only be used for images that are occasionally accessed and small in file size";
} || { printf ${SINGLE} "$1 does not exist"; exit 1; }
#end function
}

## Clean old html files
clean_output() {
printf ${DOUBLE} "Cleaning all html files ${FOLDERS[0]}"

#TODO just use find command. makes more 
find ${FOLDERS[0]} -type f -exec rm -rf {} \;

find ${FOLDERS[0]}/* -type d -exec rmdir {} \;

#remove empty folders
#rmdir ${FOLDERS[0]}/*/*
#rmdir ${FOLDERS[0]}/*/*/*

}

#Server
httpd_server () {
#go to HTML output folder
cd ${FOLDERS[0]}; 
#static python server (safer and more simple option as supposed to netcat)
python -m SimpleHTTPServer "${1:-8080}" #start on 8080 unless specified
}

#Generate sample template
#TODO: use a more generic sample template rather than bootstrap
gen_sample () {
#if TEMPLATE/header,menu,comment,footer does not exist make a basic html5 bootstrap site
#also add any necessary CSS and JS

## header.html template
test -f "${FOLDERS[1]}/${BODY[0]}" || { printf ${SINGLE} "${FOLDERS[1]}/${BODY[0]} is missing.";
cat <<EOF > ${FOLDERS[1]}/${BODY[0]}
<!DOCTYPE html>
<html lang="en">
<head>
<title>{{SITE_NAME}} | {{PAGE}}</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1" shrink-to-fit="no">
<meta name="description" content="">
<meta name="author" content="">

<link rel="stylesheet" href="../../assets/style.css">

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">

</head><body>

<nav class="nav" tabindex="-1" onclick="this.focus()">
<div class="container">
<a name="top" class="pagename" href="{{SITE_URL}}"> {{SITE_NAME}} </a>

EOF
printf ${DOUBLE} "${FOLDERS[1]}/${BODY[0]} written." ; }

# menu.html template
# TODO: automate nav links
# find nav_page.md && add to menu.html
test -f "${FOLDERS[1]}/${BODY[1]}" || { printf ${SINGLE} "${FOLDERS[1]}/${BODY[1]} is missing.";
cat <<EOF > ${FOLDERS[1]}/${BODY[1]}
<a href="../../index.html">Home</a> 

<a href="../../about.html">About</a>

<a href="../../contact.html">Contact</a>


<!--End Menu-->
</div>
</nav>
<button class="btn-close btn btn-sm">×</button>

<!-- page content starts here -->
<div class="container">
<div class="row">
<div class="col c9">

EOF
printf ${DOUBLE} "${FOLDERS[1]}/${BODY[1]} written." ; }

#comment.html template
test -f "${FOLDERS[1]}/${BODY[2]}" || { printf ${SINGLE} "${FOLDERS[1]}/${BODY[2]} is missing.";
cat <<EOF > ${FOLDERS[1]}/${BODY[2]}
<!--end main content-->
</div>

<div class="col c3">

<h3>Links</h3>

<div class="row">
  <div class="col c6"><a href="../../about.html#archives">Archives</a></div>
  <div class="col c6"><a href="../../about.html#clone">Clone</a></div>
  <div class="col c6"><a href="../../about.html#pull">Pulls</a></div>
  <div class="col c6"><a href="../../about.html#git">Git</a></div>
  <div class="col c6"><a href="../../about.html#gpg">GPG</a></div>
  <div class="col c6"><a href="../../about.html#lfs">Git annex / LFS</a></div>
</div>
<br>

<b>Search</b>            
<form class="inline smooth" action="https://duckduckgo.com/" method="POST">
<input class="smooth" type="text" placeholder="Search" aria-label="Search" name="q">
<input class="smooth" type="hidden" name="sites" value="https://example.com">
</form>
            
<br>
            
<div class="row">
  <div class="col c6"><a href="../../about.html#archives"><i class="fab fa-btc"></i></a></div>
  <div class="col c6"><a href="../../about.html#clone"><i class="fab fa-paypal"></i></a></div>
  <div class="col c6"><a href="../../about.html#pull"><i class="fab fa-gratipay"></i></a></div>
  <div class="col c6"><a href="../../about.html#git"><i class="fab fa-ethereum"></i></a></div>
  <div class="col c6"><a href="../../about.html#gpg"><i class="fab fa-patreon"></i></a></div>
</div>

</div>

<!--end of container-->
</div>
<!-- page content ends here -->




<!-- code for comments here -->
EOF
printf ${DOUBLE} "${FOLDERS[1]}/${BODY[2]} written." ; }

test -f "${FOLDERS[1]}/${BODY[3]}" || { printf ${SINGLE} "${FOLDERS[1]}/${BODY[3]} is missing.";
cat <<EOF > ${FOLDERS[1]}/${BODY[3]}
<footer class="container">
<hr>                                                                              

<p>{{PAGE}} generated {{DATE}}. <a href="#top">Back to top</a></p>                                                                               

<p>&copy; 2017-2018 {{SITE_NAME}} &middot; <a href="../../privacy.html">Privacy</a> &middot; <a href="{{GIT_SOURCE}}">Source</a></p> 

</footer>

<script src="../../assets/main.js"></script>

</body></html>

EOF
printf ${DOUBLE} "${FOLDERS[1]}/${BODY[3]} written." ; }

test -f "${FOLDERS[1]}/docs.txt" || { printf ${SINGLE} "${FOLDERS[2]}/${SOURCE_TEMPLATE[2]}/md.css is missing.";
cat <<EOF > ${FOLDERS[1]}/docs.txt
http://mincss.com/docs.html
EOF
printf ${DOUBLE} "${FOLDERS[1]}/docs.txt written." ; }

#Source/assets/md.css (special tricks for mark down)
test -f "${FOLDERS[2]}/${SOURCE_TEMPLATE[2]}/style.css" || { printf ${SINGLE} "${FOLDERS[2]}/${SOURCE_TEMPLATE[2]}/md.css is missing.";
cat <<EOF > ${FOLDERS[2]}/${SOURCE_TEMPLATE[2]}/style.css
/*MinCSS*/
body,textarea,input,select{background:0;border-radius:0;font:16px sans-serif;margin:0}.smooth{transition:all .2s}.btn,.nav a{text-decoration:none}.container{margin:0 20px;width:auto}label>*{display:inline}form>*{display:block;margin-bottom:10px}.btn{background:#999;border-radius:6px;border:0;color:#fff;cursor:pointer;display:inline-block;margin:2px 0;padding:12px 30px 14px}.btn:hover{background:#888}.btn:active,.btn:focus{background:#777}.btn-a{background:#0ae}.btn-a:hover{background:#09d}.btn-a:active,.btn-a:focus{background:#08b}.btn-b{background:#3c5}.btn-b:hover{background:#2b4}.btn-b:active,.btn-b:focus{background:#2a4}.btn-c{background:#d33}.btn-c:hover{background:#c22}.btn-c:active,.btn-c:focus{background:#b22}.btn-sm{border-radius:4px;padding:10px 14px 11px}.row{margin:1% 0;overflow:auto}.col{float:left}.table,.c12{width:100%}.c11{width:91.66%}.c10{width:83.33%}.c9{width:75%}.c8{width:66.66%}.c7{width:58.33%}.c6{width:50%}.c5{width:41.66%}.c4{width:33.33%}.c3{width:25%}.c2{width:16.66%}.c1{width:8.33%}h1{font-size:3em}.btn,h2{font-size:2em}.ico{font:33px Arial Unicode MS,Lucida Sans Unicode}.addon,.btn-sm,.nav,textarea,input,select{outline:0;font-size:14px}textarea,input,select{padding:8px;border:1px solid #ccc}textarea:focus,input:focus,select:focus{border-color:#5ab}textarea,input[type=text]{-webkit-appearance:none;width:13em}.addon{padding:8px 12px;box-shadow:0 0 0 1px #ccc}.nav,.nav .current,.nav a:hover{background:#000;color:#fff}.nav{height:24px;padding:11px 0 15px}.nav a{color:#aaa;padding-right:1em;position:relative;top:-1px}.nav .pagename{font-size:22px;top:1px}.btn.btn-close{background:#000;float:right;font-size:25px;margin:-54px 7px;display:none}@media(min-width:1310px){.container{margin:auto;width:1270px}}@media(max-width:870px){.row .col{width:100%}}@media(max-width:500px){.btn.btn-close{display:block}.nav{overflow:hidden}.pagename{margin-top:-11px}.nav:active,.nav:focus{height:auto}.nav div:before{background:#000;border-bottom:10px double;border-top:3px solid;content:'';float:right;height:4px;position:relative;right:3px;top:14px;width:20px}.nav a{padding:.5em 0;display:block;width:50%}}.table th,.table td{padding:.5em;text-align:left}.table tbody>:nth-child(2n-1){background:#ddd}.msg{padding:1.5em;background:#def;border-left:5px solid #59d}
/*CSS tricks for markdown*/
blockquote > p > hr {display: block;height: .5px;border: 0;border-top: .5px solid #ccc;margin: 1em 0;padding: 1px;overflow: auto;clear: both;}
p > em > strong {display:block;margin: 0 0 20px;position: relative;border-left: 15px solid #111;border-right: 2px solid #111;list-style-type: none;}
/*Left/Right Align img*/
blockquote > p > em > img {float: left;margin-right:5px;margin-bottom: 5px;list-style-type: none;}
blockquote > p > strong > img {float: right;margin-left:5px;margin-bottom: 5px;list-style-type: none;}
.p {overflow: auto;}
/*Social Media Buttons*/
.fab {padding: 10px;font-size: 28px;width: 48px;text-align: center;text-decoration: none;margin: 5px 2px; color:#fff;}.fab:hover {opacity: 0.7;}
.fa-facebook {background: #3B5998;}.fa-twitter {background: #55ACEE;}.fa-google {background: #dd4b39;}.fa-youtube {background: #bb0000;}.fa-linkedin {background: #007bb5;}
.fa-github {background: #24292e;}.fa-gitlab {background: #544388;color: orange;}.fa-git{background: #ccc;}
.fa-patreon {background: #f96854;}.fa-paypal {background: #0070ba;}.fa-gratipay{background: #f6c915;}
.fa-btc {background: #ff9500;}.fa-ethereum {background: #fff;color: #000;}
.fa-stackexchange{background: #242729;color: #4679b7;}
EOF
printf ${DOUBLE} "${FOLDERS[2]}/${SOURCE_TEMPLATE[2]}/md.css written." ; }


test -f "${FOLDERS[2]}/${SOURCE_TEMPLATE[2]}/main.js" || { printf ${SINGLE} "${FOLDERS[2]}/${SOURCE_TEMPLATE[2]}/md.css is missing.";
cat <<EOF > ${FOLDERS[2]}/${SOURCE_TEMPLATE[2]}/main.js
/*insert JS here*/
EOF
printf ${DOUBLE} "${FOLDERS[2]}/${SOURCE_TEMPLATE[2]}/main.js written." ; }

#end of template generation
printf ${DOUBLE} "all missing template files restored."
}

## Help
help () {
cat <<EOF
${0}
Usage: ${0} [parameter]

help                   This dialouge

page "name"          New page (edit ${0} to edit template format "\$POST_FORMAT")

post "name"          New post (edit ${0} to edit template format "\$POST_FORMAT" and "\$INDEX_FORMAT")

template               Generate a sample template for this site

generate               Generate site from ${FOLDERS[2]}

clean                  Delete html files from ${FOLDERS[0]} (Output) folders to allow re-generation (does not delete assets or other files)

base64 "file.jpg"    Get "file.jpg", minimize, and convert an image into base64 css (best used with small static images)

http 8080               Start simple http server on port 8080 to see results (default: 8080)

-----

Template Variables
  Consider setting these variables in "config.sh" in the root folder of a folder that mksitev2 has setup.

{{SITE_NAME}} = "${SITE_NAME}"
{{SITE_URL}} = "${SITE_URL}"
{{AUTHOR}} = "${AUTHOR}"
{{GIT_SOURCE}} = "${GIT_SOURCE}"
{{PAGE}} = "name" (show the name of the md file without the extention)
EOF
}

#main logic

case "$1" in

generate) move_files ; search_folders ;;

template) gen_sample ;;

page) check_input "${2}"; new_page "${2}" ;;

post) check_input "${2}"; new_post "${2}";;

clean) clean_output ;;

base64) mkbase64 "${2}" ;;

http) httpd_server "${2}" ;;

*) help ;;

esac
#EOF
