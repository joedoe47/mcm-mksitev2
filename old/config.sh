#Mksite Configuration

SITE_NAME="Mendoza Consultant"

SITE_URL="https://mendozaconsultcorp.com"
#SITE_URL="http://127.0.0.1:8080"

#leave commented if multiple users make posts
#AUTHOR="John Doe"

GRAVATAR_SIZE="90"

#files that you want copied over to the output folder
STATIC_FILES=(
"htm"
"html"
"css"
"js"
"jpeg"
"jpg"
"png"
"gif"
"bmp"
)

#the default file ending to convert into webpages
TEXT_FILE="md"

#add http freindly url if you use SSH
GIT_SOURCE="https://gitlab.com/joedoe47/mksitev2"
