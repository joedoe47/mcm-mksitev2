<div class="content-wrapper">

<div class="content">
<h2 class="content-head is-center">Excepteur sint occaecat cupidatat.</h2>

<div class="pure-g">


<div class="l-box pure-u-1 pure-u-md-1-2 pure-u-lg-1-4">
<h3 class="content-subhead">
<i class="fa fa-business-time"></i>
Fast Delivery
</h3>
<p>
Sites and code are delivered on time with agreed features or your money back.
</p>
</div>

<div class="l-box pure-u-1 pure-u-md-1-2 pure-u-lg-1-4">
<h3 class="content-subhead">
<i class="fa fa-rocket"></i>
Fast load times
</h3>
<p>
Code should always be optimized and take advantage of any technologies to be blazing fast and useable 99.999%
</p>
</div>

<div class="l-box pure-u-1 pure-u-md-1-2 pure-u-lg-1-4">
<h3 class="content-subhead">
<i class="fa fa-th-large"></i>
Modular
</h3>
Always built in a modular way to be "future proof" and "scalable" if you need to expand rapidly or to integrate with other tools for commerce, analytics, or custom apps you have.
</p>
</div>

<div class="l-box pure-u-1 pure-u-md-1-2 pure-u-lg-1-4">
<h3 class="content-subhead">
<i class="fa fa-comments"></i>
Instant Support
</h3>
<p>
Communication is key to success. Monday through Saturday 7am to 10pm I can communicate in real time as need to fix the problem in real time.
</p>
</div>

<div class="l-box pure-u-1 pure-u-md-1-2 pure-u-lg-1-4">
<h3 class="content-subhead">
<i class="fa fa-history"></i>
Edits, Revisions, & Upgrades
</h3>
<p>
projects include as many revisions and upgrades as needed and  in the outline of the project.
</p>
</div>

</div>

</div>


<div class="ribbon l-box-lrg pure-g">

<div class="l-box-lrg is-center pure-u-1 pure-u-md-1-2 pure-u-lg-2-5">
<img width="300" alt="File Icons" class="pure-img-responsive" src="/img/common/file-icons.png">
</div>

<div class="pure-u-1 pure-u-md-1-2 pure-u-lg-3-5">
<h2 class="content-head content-head-ribbon">Sample Gallery</h2>
<p>
You can view the different sample designs, frameworks, and technologies used in this gallery.
</p>
</div>

</div>


<div class="content">
<a name="contact"></a>
<h2 class="content-head is-center">Contact</h2>

<div class="pure-g">

<div class="l-box-lrg pure-u-1 pure-u-md-2-5">
<a name="submitted"></a>
 <div class="thankyou_message">
 <h2><em>Thanks</em> for contacting us!
 We will get back to you soon!</h2>
 </div>
  </form>

</div>

<div class="l-box-lrg pure-u-1 pure-u-md-3-5">
<h4>Get in touch</h4>
<p>
I'm also available for contact on the following social media platforms and post things related to work, nature, and X
<br>
</p>

<div class="pure-button-group" role="group" aria-label="...">
<a href="" class="pure-button">
<i class="fab fa-twitter-square"></i>
Twitter
</a>

<a href="" class="pure-button">
<i class="fab fa-linkedin"></i>
Linkedin
</a>

<a href="" class="pure-button">
<i class="fab fa-youtube"></i>
Youtube
</a>
</div>

</div>



</div>

</div>


