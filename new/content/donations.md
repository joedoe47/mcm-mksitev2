## Donations

Running a few tor nodes, hosting this server, giving back to the open source community, and doing cool things costs money. Any donations are greatly appreciated.


- Flattr: [FAQ](https://flattr.com/faq) | [sign up](https://flattr.com/settings/account/subscription)

- BTC: 1KShmEaUxRkkPEgTSNoFLZaUyEC6S32KZ7

- ETH: ETH: 0x40626fFF7C7498c7FA15A91520DB356D17B2d993

- LTC: LQ3PoZaVU7bZ3V879KZsg4KhbiFZmkFcet


