

I particularly like to use the i3 window manager on my local linux systems because it focuses on using the keyboard and allows me to be super productive and seems less taxing than xfce or lxde, even on my raspberry pi. However using the new PIXL desktop for the raspberry pi make it a little difficult to change the session you log into.

If you are such a person who wants to use a more keyboard focused window manager or want something a little lighter than lxde, perhaps i3 is for you. If you are not cozy with i3 there are certainly other window managers you can try like awesome, byobu, and so on. (so this might be help for for you sitation also)

If you are still a person that uses GUIs that resemble say like windows/OSx UIs you may want to try to find and use the keyboard shortcuts for your particular operating system to see just how much faster you can do things. It will get some time getting used to remember certain keyboard shortcuts but in the long run I believe its worth it to memorize some shortcuts you may find useful to save a bunch of time, even if you use something like KDE or Gnome or even Windows or OsX. 



n.b. any text you see after "$" indicates it is a command you need to run in a terminal


Steps to reproduce if you mainly use the ***GUI***:

- Boot rasperry pi and log into raspbian

- Open a terminal

- Update and install i3
$ sudo apt update & sudo apt install i3 dmenu suckless-tools

- Change the greeter so we can select i3 and log into i3 (or not when we don't want to use it)
$ sed -i s/greeter-session=pi-greeter/greeter-session=lightdm-gtk-greeter/g /etc/lightdm/lightdm.conf

- Disable auto log in via raspi-config
$ sudo raspi-config

- Select "Boot Options"

- Select "Desktop/CLI"

- Select "Desktop"

- Reboot

- Profit! :D



Steps to reproduce if you use ___console___ interface most of the time and occasionally use a GUI:

- Boot raspberry pi and log into raspbian

- Update and install i3
$ sudo apt update & sudo apt install i3 dmenu suckless-tools

- Add i3 to our xinit.rc file so startx will run i3 
$ echo "exec i3" > .xinit.rc 

- Start xserver 
$ startx

- Profit! :D

Reminder: if you are using the Console interface you need to type "startx" every time after boot to use i3.

----

Now all thats left to do is set up the modifier, a key you will press to indicate to i3 that you wish to do something. Sort of how on windows you press "Alt"; "Alt" would be considered the modifier you would press to run a certain shortcut command. 

There is a full (i3 userguide)[https://i3wm.org/docs/userguide.html] that descibes more of what
you can do in i3; from moving windows, setting custom keyboard commands, to even making sure firefox opens up on the right screen on our dual screen set up and in full screen to minimize disctractions.

